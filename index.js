// Importamos la función para descargar imágenes
const imageDownloader = require('./image-downloader').download;

// URL de la imagen que queremos descargar
const imageUrl = 'https://sitelabs.es/wp-content/uploads/2016/06/regex-regular-expression.jpg';

// Fichero de salida con el directorio al que vamos a guardar
const filename = 'images/'.concat('regular-expresion.jpg');

// Función para descargar las imágenes
imageDownloader(imageUrl, filename, function(){
    console.log(`${imageUrl} image download!!`);
});